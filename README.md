# VIM Notes

### Searching

```
:%s/donut//gn                               : Count how many times donut occur in a file
:%s///gn                                    : Count the matches of your MOST RECENT search
:%!awk '{print $2,$3}'                      : To format with awk
:%!jq                                       : To format a json file using jq like this
/joe/e                                      : Cursor set to End of match
/joe/+3                                     : Find joe move cursor 3 lines down
/^joe.*fred.*bill/                          : Find joe AND fred AND Bill (Joe at start of line)
/begin\_.*end                               : Search over possible multiple lines
/.*fred\&.*joe                              : Search for FRED AND JOE in any ORDER!
/\<fred\>/                                  : Search for fred but not alfred or frederick
/\<\d\d\d\d\>                               : Search for exactly 4 digit numbers
/\<\d\{4}\>                                 : Same thing
/pancake\|waffle                            : Search for either a pancake or a waffle
/\vpancake|waffle                           : Same thing
```

### Global command

```
:g/fred.*joe.*dick/                         : Display all lines fred,joe & dick
:g/\<fred\>/                                : Display all lines fred but not freddy
:g/^\s*$/d                                  : Delete all blank lines
:g!/^dd/d                                   : Delete lines not containing string
:v/^dd/d                                    : Same thing
:g/joe/,/fred/d                             : Not line based (very powerfull)
:g/fred/,/joe/j                             : Join Lines
:g/^$/d                                     : Delete empty lines
:g/-------/.-10,.d                          : Delete string & 10 previous lines
:g/{/ ,/}/- s/\n\+/\r/g                     : Delete empty lines but only between {...}
:v/\S/d                                     : Delete empty lines (and blank lines ie whitespace)
:v/./,/./-j                                 : Compress empty lines
:g/^$/,/./-j                                : Compress empty lines
:g/<input\|<form/p                          : ORing
:g/^/put_                                   : Double space file (pu = put)
:g/^/m0                                     : Reverse file (m = move)
:'a,'bg/^/m'b                               : Reverse a section a to b
:g/^/t.                                     : Duplicate every line
:g/fred/t$                                  : Copy (transfer) lines matching fred to EOF
:g/stage/t'a                                : Copy (transfer) lines matching stage to marker a (cannot use .)
:g/pattern/s/^/#/g                          : Comment out lines containing pattern
:g/pattern/m$                               : Gathering up lines and moving them together
```

### Substitutions

```
:s/\<./\u&/g                                : Capitalize first letter of each word
:s/&/\r/g                                   : Substitute all instances of & to newline (\r)
:%s/\s\+$//                                 : Delete all trailing whitespace
:v/AA`/s/^/\* /                             : Substitute beginning of lines not matching `AA` by `* `
:%s/.*/\L&                                  : Lowercase the entire file
:s/.*/(&)/                                  : Put current line into brackets
:%s/pattern/mv & &.old/                     : Swap pattern with mv pattern pattern.old
:%s/\(one\) \(two\) \(three\)/\3 \1 \2/     : Swap one two three with three one two
:%s/   .*$//                                : Delete all after 3 spaces
:%s/JPG .*$//                               : Delete all after JPG in DSCN1120.JPG 02-Apr-2006 20:27 279k
:s/\(.*\)-\(.*\)/\2-\1/                     : Swap two fields separated by a dash
:%s/^......//`                              : Delete first 6 characters of each line (or `:%s/^.\{6\}//` )
:%s/....$//`                                : Delete last 4 characters of each line (or `:%s/.\{4\}$//`)
```

### Moves with lines

```
:2,8co15                                    : Copy lines 2 through 8 after line 15
:4,15t$                                     : Copy lines 4 through 15 to end of document (t == co)
:-t$                                        : Copy previous line to end of document
:m0                                         : Move current line to line 0
:.,+3m$-1                                   : Current line through current+3 are moved to the lastLine-1
:-10t.                                      : Copy from 10 lines above to the current line
:+5t.                                       : Copy from 5 lines below to the current line
:1,10g/^/12,17 t $                          : Copy 10 times lines 12 through 17 at the end of file
:-3,-1s#a#X#g                               : From 3 lines above to 1 line above the cursor
```

### Vim to archive files

`vim archive.tar.gz` will list archived files, which you can edit/save without extracting.
(Supported: tar.gz, tgz, zip, etc.)

### To display the ASCII value of the character under cursor

press `ga`

### Open your .vimrc in a vertical split

`:vsp $MYVIMRC`

### To split an existing buffer vertically

`:vert sb#` (where # is the buffer number)

### Count words and lines

To get information about the current cursor position, including the current line and total number of lines plus the total number of words in the file and more `g C-g`

### Vim has 5 different histories

```
:his c - cmd-line history
:his s - search history
:his e - expression history
:his i - input history
:his d - debug history
```

### Misc operations

```
Use / after c, d, or y, to operate until a pattern:
• c/\d - change to next number
• d/foo - delete forward until foo
• y/pattern – yank everything up until pattern

Quick replacements:
• ci} - Change inside braces
• cf, - Change forward including comma
• caw – Change around word, including trailing whitespace
• ciw – Change inside word, excluding whitespace
• di" - Delete inside quotes
• diw - Delete inside word
• dtc - Delete to next c

Efficient changes with text objects:
• da" - Delete Around quotes
• di] - Delete Inside brackets
• ci{ - Change Inside braces
• dap - Delete Around Paragraph
• vaw - Visually select Around Word
• di" - Delete text within double quotes.
• di' - Delete text within single quotes.
• di` - Delete text within backquotes.
• yi" - Yank (copy) text within double quotes.
• dib - Delete text within brackets ()
• diB - Delete text within Brackets {}
```

### Digraphs (special chars)

`:digraphs` or `:dig` to list them
while in insert mode, hit `C-k` and enter the digraph code (first column)
ex: `C-k 12` to get ½
for help : `:h digraphs`

### Changing case in Vim

Visual select the text, then U for uppercase or u for lowercase. To swap all casing in a visual selection, press ~.

Without using a visual selection, gU<motion> will make the characters in motion uppercase, or use gu<motion> for lowercase.

```
~    : Changes the case of current character
guu  : Change current line from upper to lower
gUU  : Change current LINE from lower to upper
guw  : Change to end of current WORD from upper to lower
guaw : Change all of current WORD to lower
gUw  : Change to end of current WORD from lower to upper
gUaw : Change all of current WORD to upper
g~~  : Invert case to entire line
g~w  : Invert case to current WORD
guG  : Change to lowercase until the end of document
gU)  : Change until end of sentence to upper case
gu}  : Change to end of paragraph to lower case
gU5j : Change 5 lines below to upper case
gu3k : Change 3 lines above to lower case
```

### YAML files starter pack

```
:set paste
:set shiftwidth=2
:set autoindent
```

`echo "autocmd FileType yaml,yml setlocal ai ts=2 sw=2 et" >> ~/.vimrc`

### Visual block

To save a block selection to a file : `:'<,'>w new.txt`

### Formating JSON

`:%!python3 -m json.tool`

### Save vimdiff output to html

`vimdiff file1 file2 -c TOhtml -c 'w! vimdiff.html' -c 'qa!'`

### How to install a plugin with Vundle

1. Add the following configuration to your `.vimrc`.

    Plugin 'vim-airline/vim-airline'

2. Install with `:PluginInstall`

3. List plugins with `:PluginList`

